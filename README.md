# openFrameworks & Arduino Serial Communication Program
- openFrameworks와 Arduino를 연결해서 데이터를 주고 받는 프로그램
- 데이터를 어떻게 주고 받는지 보여주기 위한 테스트 프로그램

## Download & Install
1. GitLab에서 ZIP 파일을 다운 받는다.
1. 압축을 푼다.
1. 실제로 사용할 것은 src 폴더의 3개 파일과 arduinoSerial 폴더에 있는 1개 파일이다. 파일이 있는지 확인한다.

## openFrameworks Setup
1. 오픈프레임웍스 제너레이터에서 신규 프로젝트를 만든다.
1. 프로젝트 이름을 정하고 AddOn 에서 ofxGui 를 선택한다.
1. 폴더 위치를 지정하고 Generate 한다.
1. Xcode에서 해당 프로젝트를 Open
1. openFrameworks / Debug / Release 선택 중에서 Debug 또는 Release 를 선택한다.
1. 아무것도 하지 않은 상태에서 RUN 한다. (실행이 되는지 확인하기 위함)
1. 이상 없이 실행된다면 프로그램 종료, Xcode도 종료
1. 프로젝트의 src 폴더에 있는 3개 파일을 다운 받은 것으로 교체한다. (물론, src 폴더에 있던 3개 파일로 바꾼다)
1. 다시 Xcode를 열고 RUN 돌려 본다. (이번에는 어플이 동작하는지 보기 위함)
1. RGB 각 슬라이드를 움직여 보고, BLINK 체크 버튼도 동작시켜 본다.
1. 제대로 동작하면 Xcode 쪽은 완료

## Arduino Setup
1. Xcode를 종료한 상태에서 작업한다.
1. 다운로드 받은 것 중에서 arduinoSerial 폴더의 .ino 파일을 Arduino 프로그램에서 Open 한다.
1. 아두이노 연결하고 아두이노에 업로드 한다.
1. 이상 없이 업로드 되면 정상, 

## Arduino Hardware Setup
1. 회로도 보기 https://gitlab.com/idnine/oF-Arduino-Serial-Test/blob/master/circuit.jpg
1. Pullup Switch to pin 7
1. Green Led pin 6
1. Red Led pin 5
1. Knob Volume pin A0

## 동작 설명
1. 아두이노의 Knob를 돌리면 openFrameworks의 Speed 페이더가 움직인다.
1. 아두이노의 버튼을 누를 때 마다 openFrameworks의 Blink 체크박스가 토글된다.
1. 아두이노의 LED는 openFrameworks에서 보내주는 값에 따라 색깔이 바뀌고 깜빡인다.
