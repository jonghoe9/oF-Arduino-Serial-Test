int btnPin = 7;
int ledRedPin = 6;
int ledGreenPin = 5;
int volPin = A0;

int colorGreen = 255;
int colorRed = 0;
int volOld = 0;
bool toggle = false;

String serialBuff = "";
bool serialComplete = false;

void setup() {
	pinMode(btnPin, INPUT);
	pinMode(ledRedPin, OUTPUT);
	pinMode(ledGreenPin, OUTPUT);
	Serial.begin(9600);
	serialBuff.reserve(100);
}

void loop() {
	// Volume Read
	int volVal = map(1023 - analogRead(volPin), 0, 1023, 60, 20);
	if(volVal != volOld) {
		volOld = volVal;
		Serial.print("/knob ");
		Serial.print(volVal);
		Serial.print("\n");
	}
	// Button Read
	int btnRead = digitalRead(btnPin);
	if((btnRead == LOW) && (toggle == false)) {
		toggle = true;
		Serial.print("/toggle 1");
		Serial.print("\n");
	} else if((btnRead == HIGH) && (toggle == true)) {
		toggle = false;
	}
	// Serial Read
	if(serialComplete) {
		String buffCol, buffVal;
		int idx = serialBuff.indexOf(' ', 0);
		buffCol = serialBuff.substring(0, idx);
		buffVal = serialBuff.substring(idx+1);
		int newVal = buffVal.toInt();
		
		if(buffCol == "/red") {
			colorRed = newVal;
		}
		if(buffCol == "/green") {
			colorGreen = newVal;
		}
		serialBuff = "";
		serialComplete = false;
	}
	// LED Display
	analogWrite(ledRedPin, colorRed);
	analogWrite(ledGreenPin, colorGreen);
}

void serialEvent() {
	while (Serial.available()) {
		char inChar = (char)Serial.read();
		serialBuff += inChar;
		if (inChar == '\n') {
			serialComplete = true;
		}
	}
}

