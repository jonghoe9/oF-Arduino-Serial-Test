#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetFrameRate(60);
	ofBackground(255, 255, 255);
	
	// =========================================
	//               ARDUINO OPEN
	// =========================================
	serial.listDevices();
	vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
	for (int i = 0; i < deviceList.size(); i++){
		serialName = deviceList[i].getDeviceName().c_str();
		if(serialName.substr(0,12) == "tty.usbmodem") {
			serialNo = i;
		}
	}
	serial.setup(serialNo, serialBps);
	serialName = deviceList[serialNo].getDeviceName().c_str();
	
	// =========================================
	//                 DISPLAY GUI
	// =========================================
	guiPara.setName(serialName);
	guiPara.add(red.set("LED RED", 0, 0, 255));
	guiPara.add(green.set("LED GREEN", 255, 0, 255));
	guiPara.add(blue.set("LED BLUE", 0, 0, 255));
	guiPara.add(speed.set("SPEED", 35, 60, 20));
	guiPara.add(blink.set(" BLINK", false));
	gui.setup(guiPara);
	
}

//--------------------------------------------------------------
void ofApp::update(){
	// =========================================
	//          SERIAL DATA FROM ARDUINO
	// =========================================
	while(serial.available()) {
		int c = serial.readByte();
		if(c == '\n') {
			vector<string> serialData = ofSplitString(serialBuff, " ");
			if(serialData.size() > 1) {
				if(serialData[0] == "/knob") {
					speed = ofToInt(serialData[1]);
				} else if(serialData[0] == "/toggle") {
					if(blink) {
						blink = false;
						oldRed = 0;
						oldGreen = 0;
						oldBlue = 0;
					} else {
						blink = true;
					}
				}
			}
			serialBuff = "";
		} else {
			serialBuff.push_back(c);
		}
	}
	// =========================================
	//            SERIAL DATA TO ARDUINO
	// =========================================
	if(blink) {
		if(cntFrame > speed) {
			// Blink Timing Count
			cntFrame = 0;
			ledStatus ^= 1;
			
			// LED STATUS MESSAGE SEND
			if(ledStatus) {
				serialMessage("/red", red);
				serialMessage("/green", green);
				serialMessage("/blue", blue);
			} else {
				serialMessage("/red", 0);
				serialMessage("/green", 0);
				serialMessage("/blue", 0);
			}
		} else {
			cntFrame++;
		}
	} else {
		ledStatus = true;
		if(oldRed != red) {
			oldRed = red;
			serialMessage("/red", oldRed);
		}
		if(oldGreen != green) {
			oldGreen = green;
			serialMessage("/green", oldGreen);
		}
		if(oldBlue != blue) {
			oldBlue = blue;
			serialMessage("/blue", oldBlue);
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw(){
	gui.draw();
	
	ofNoFill();
	ofSetColor(ofColor::black);
	ofDrawRectangle(222, 10, 114, 114);
	
	ofFill();
	if(blink && !ledStatus) {
		ofSetColor(ofColor::lightGray);
	} else {
		ofSetColor(red, green, blue);
	}
	ofDrawRectangle(224, 12, 110, 110);
}

//--------------------------------------------------------------
void ofApp::serialMessage(string addr, int val) {
	string msg;
	unsigned char* msguc;
	msg = addr + " " + ofToString(val) + "\n";
	msguc = new unsigned char[msg.size()];
	memcpy(msguc, msg.c_str(), msg.size());
	serial.writeBytes(msguc, msg.size());
	delete [] msguc;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
