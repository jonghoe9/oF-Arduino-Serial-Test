#pragma once

#include "ofMain.h"
#include "ofXGui.h"

class ofApp : public ofBaseApp{
	// GUI
	ofxPanel gui;
	ofParameterGroup guiPara;
	ofParameter<int> red;
	ofParameter<int> green;
	ofParameter<int> blue;
	ofParameter<int> speed;
	ofParameter<bool> blink;
	
	// SERIAL
	ofSerial serial;
	string serialName, serialBuff;
	int serialBps = 9600;
	int serialNo;
	
	// BLINK
	int cntFrame;
	int oldRed, oldGreen, oldBlue;
	bool ledStatus;
	
	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
	void serialMessage(string addr, int val);
};
